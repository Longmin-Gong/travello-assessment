package com.longmin.market.data;

import com.longmin.market.product.ProductItem;
import com.longmin.market.rule.PricingRule;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Longmin
 * <p>
 * Data factory which is used to initial product item and pricing rules.
 */
public class DataFactory {

  /**
   * Initialize 2 normal pricing rules.
   *
   * @return the list of {@link PricingRule}
   */
  public static List<PricingRule> initialNormalPricingRulList() {
    return new ArrayList<PricingRule>() {{
      add(new PricingRule(new ProductItem("A", 50, "cent"), 3, 130, "cent"));
      add(new PricingRule(new ProductItem("B", 30, "cent"), 2, 45, "cent"));
    }};
  }

  /**
   * Initialize two pricing rules for one {@link ProductItem}.
   *
   * @return the list of {@link PricingRule}
   */
  public static List<PricingRule> initialDuplicatePricingRulList() {
    return new ArrayList<PricingRule>() {{
      add(new PricingRule(new ProductItem("A", 50, "cent"), 2, 90, "cent"));
      add(new PricingRule(new ProductItem("A", 50, "cent"), 4, 160, "cent"));
    }};
  }
}
