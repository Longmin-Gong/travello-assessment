package com.longmin.market;

import com.longmin.market.data.DataFactory;
import com.longmin.market.product.ProductItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 * Test class for {@link Checkout}
 */
class CheckoutTest {

  /**
   * No special pricing offers.
   */
  @Test
  protected void testNormalCaseWithoutPricingRules() {
    Checkout checkout = new Checkout(new ArrayList<>());
    checkout.scan(new ProductItem("A", 50, "cent"));
    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(100, checkout.total());

    checkout.scan(new ProductItem("B", 30, "cent"));
    checkout.scan(new ProductItem("B", 30, "cent"));
    Assertions.assertEquals(160, checkout.total());

    checkout.scan(new ProductItem("C", 20, "cent"));
    Assertions.assertEquals(180, checkout.total());
  }

  /**
   * Special offers:
   * #1 3 A -> 130, 1 A -> 50
   * #2 2 B -> 45, 1B -> 30
   */
  @Test
  protected void testNormalCaseWithPricingRules() {
    Checkout checkout = new Checkout(DataFactory.initialNormalPricingRulList());

    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(50, checkout.total());

    checkout.scan(new ProductItem("B", 30, "cent"));
    Assertions.assertEquals(80, checkout.total());

    checkout.scan(new ProductItem("C", 30, "cent"));
    Assertions.assertEquals(110, checkout.total());

    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(160, checkout.total());

    checkout.scan(new ProductItem("B", 30, "cent"));
    Assertions.assertEquals(175, checkout.total());

    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(205, checkout.total());

    checkout.scan(new ProductItem("A", 50, "cent"));
    checkout.scan(new ProductItem("A", 50, "cent"));
    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(335, checkout.total());
  }

  /**
   * Special offers:
   * #1 2 A -> 90, 4 A -> 160, 1 A -> 50
   */
  @Test
  protected void testDuplicatePricingRuleCase() {
    Checkout checkout = new Checkout(DataFactory.initialDuplicatePricingRulList());

    checkout.scan(new ProductItem("A", 50, "cent"));
    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(90, checkout.total());

    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(140, checkout.total());

    checkout.scan(new ProductItem("A", 50, "cent"));
    Assertions.assertEquals(160, checkout.total());
  }
}
