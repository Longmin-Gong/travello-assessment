package com.longmin.market.rule;

import com.longmin.market.product.ProductItem;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Longmin
 * <p>Pricing rule is a special offer for certain number of product</p>
 */

@Data
@AllArgsConstructor
public class PricingRule implements Comparable<PricingRule> {

  private ProductItem productItem;

  private int count;

  private int specialPrice;

  private String unit;

  /**
   * Implement {@link Comparable interface} to sort by desc.
   * @param target
   * @return
   */
  @Override
  public int compareTo(PricingRule target) {
    if (this.count > target.count) {
      return -1;
    } else if (this.count == target.count) {
      return 0;
    } else {
      return 1;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PricingRule that = (PricingRule) o;
    return count == that.count
        && specialPrice == that.specialPrice
        && Objects.equals(productItem, that.productItem)
        && Objects.equals(unit, that.unit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productItem, count, specialPrice, unit);
  }
}
