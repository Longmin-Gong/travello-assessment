package com.longmin.market.product;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Longmin
 * <p>Product item which contains product name, price, and unit.</p>
 */
@Data
@AllArgsConstructor
public class ProductItem {

  private String name;

  private Integer price;

  private String unit;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductItem that = (ProductItem) o;
    return Objects.equals(name, that.name)
        && Objects.equals(price, that.price)
        && Objects.equals(unit, that.unit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, price, unit);
  }
}
