package com.longmin.market;

import com.longmin.market.product.ProductItem;
import com.longmin.market.rule.PricingRule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Longmin
 * @date 19/02/2021
 */
public class Checkout {

  /**
   * Scanned product item map.
   */
  private Map<ProductItem, Integer> productMap;

  /**
   * Calculated price map for each scanned product item.
   */
  private Map<ProductItem, Integer> pricingMap;

  /**
   * Pricing rules map for each product item.
   */
  private Map<ProductItem, List<PricingRule>> pricingRuleMap;

  public Checkout(List<PricingRule> pricingRuleList) {
    this.pricingRuleMap = initPricingRuleMap(pricingRuleList);
    this.productMap = new HashMap<>();
    this.pricingMap = new HashMap<>();
  }

  /**
   * Init pricing rule map for each product item.
   *
   * @param pricingRuleList list of {@link PricingRule}
   * @return pricing rule map
   */
  private Map<ProductItem, List<PricingRule>> initPricingRuleMap(List<PricingRule> pricingRuleList) {
    Map<ProductItem, List<PricingRule>> ruleMap = new HashMap<>();
    pricingRuleList.forEach(pricingRule -> {
          if (ruleMap.containsKey(pricingRule.getProductItem())) {
            ruleMap.get(pricingRule.getProductItem()).add(pricingRule);
          } else {
            List<PricingRule> pricingRules = new ArrayList<>();
            pricingRules.add(pricingRule);
            ruleMap.put(pricingRule.getProductItem(), pricingRules);
          }
        }
    );
    return ruleMap;
  }

  /**
   * Scan {@link ProductItem} when checkout.
   *
   * @param item {@link ProductItem}
   */
  public void scan(ProductItem item) {
    // increase the count of product item
    productMap.put(item, productMap.getOrDefault(item, 0) + 1);
    // increase the product count when scanning the item
    update(item);
  }

  /**
   * Update the total price dynamic.
   *
   * @param item {@link ProductItem}
   */
  private void update(ProductItem item) {
    int count = productMap.get(item);
    int amount = 0;
    List<PricingRule> pricingRuleList = pricingRuleMap.get(item);
    if (pricingRuleList != null) {
      // sort pricing rules for one product item, then calculate for maximum special prices.
      Collections.sort(pricingRuleList);
      for (PricingRule pricingRule : pricingRuleList) {
        int div = count / pricingRule.getCount();
        if (div > 0) {
          count -= div * pricingRule.getCount();
          amount += div * pricingRule.getSpecialPrice();
        }
      }
    }
    if (count > 0) {
      amount += item.getPrice() * count;
    }
    this.pricingMap.put(item, amount);
  }

  /**
   * Count all product prices.
   *
   * @return the total amount for checkout products.
   */
  public long total() {
    return pricingMap.values().stream().mapToInt(i -> i.intValue()).sum();
  }
}
