# travello-assessment

This is a coding assessment for Travello.

**Supermarket Checkout**

Implement the code for a supermarket checkout that calculates the total price of a number of items,
the system should handle pricing schemes such as “apples cost 50 cents, three apples cost $1.30.”
In a normal supermarket, things are identified using Stock Keeping Units, or SKUs. In our store, we’ll
use individual letters of the alphabet (A, B, C, and so on). Our goods are priced individually. In
addition, some items are multi-priced: buy n of them, and they’ll cost you y cents. For example, item
‘A’ might cost 50 cents individually, but this week we have a special offer: buy three ‘A’s and they’ll
cost you $1.30. In fact this week’s prices are:

| Item | Unit Price | Special Price |
| ------ | ------ | ------ |
| A | 50 | 3 for 130 |
| B | 30 | 2 for 45 |
| C | 20 | |
| D | 15 | |

Our checkout accepts items in any order, so that if we scan a B, an A, and another B, we’ll recognize
the two B’s and price them at 45 (for a total price so far of 95). Because the pricing changes
frequently, we need to be able to pass in a set of pricing rules each time we start handling a checkout
transaction.
The interface to the checkout should look like this:

```
Checkout checkout = new Checkout(pricingRules)
checkout.scan(item)
checkout.scan(item)
…..
Price price = checkout.total()
```

### How to build the project
```
./mvnw clean package
```

### How to execute the project
```
Checkout checkout = new Checkout(pricingRules); // pricingRules -> List<PricingRule>
checkout.scan(item); // item -> ProductItem
...
int totalPrice = checkout.total();
```

### Third-party maven dependencies
  - Junit (5.4.0)
  - Lombook (1.18.18)

### Static analysis maven plugins
  - maven-pmd-plugin
  - maven-checkstyle-plugin
  
### Unit test scenarios: CheckoutTest.java
  - No pricing rules
  - Single pricing rule for each product item
  - Multiple pricing rules for each product item
